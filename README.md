# Uni-LDAP
```
Pathway I, Practical I
ONT4202  [4206] 
Nelson Mandela University (2020)
Design Pattern : Iterator
```
<br/>
Console Application used by a university to manage the storing of student and staff information in the active directory.  

Logically, the application maintains two lists; **Staff Members**, and **Students**.
Using the Iterator pattern, the application is able to technically combine these two lists into one object.

The application provides means for the elements of this aggregated list to be accessed sequentially, while implementation remains encapsulated.
<br/><br/>
![UML Class](https://cloud.owncube.com/apps/gallery/preview.public/58636567?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)

**Remote Repository :** [https://gitlab.com/Cosurmyne/uni-ldap.git](https://gitlab.com/Cosurmyne/uni-ldap.git)

<br/><br/><br/>
## Documentation
The application kicks start with the **Subject** class, to encapulate both, staff and student records.  

```cs
public class Subject
{
	private string fName;
	private string lName;
	private byte age;
	private string dept;
	private string field;
	private byte years;

	public Subject(string name, string sName, byte age, string dept, string field, byte yrs)
	{
		this.fName = name;
		this.lName = sName;
		this.age = age;
		this.dept = dept;
		this.field = field;
		this.years = yrs;
	}

	public string FName { get{ return fName; } }
	public string LName { get{ return lName; } }
	public byte Age { get{ return age; } }
	public string Dept { get{return dept; } }
	public string Field { get{ return field; } }
	public byte Years { get{ return years; } }

	public override string ToString()
	{
		return String.Format("{0, -20} {1, -20} ({2}) : {3}", FName, LName, Age, Dept);
	}
}
```

### Abstraction
These two subject types are then aggregated inside repective classes, implementing the same interface.  

```cs
public interface AggregateSubject
{
	Iterator CreateIterator();
	byte Count();
	void Add(Subject o);
}
```

This interface is then linked to another, to handle the actual iterating.

```cs
public interface Iterator
{
	Subject First();
	Subject Next();
	Subject Current();
	bool IsDone();
}
```
<br/><br/>
<br/><br/>
### Aggregating Subjects
The **AggregateSubject** interface is implemented in two classes, with code baring heavy resemblense.  

#### Aggregate Staff

```cs
public class Staff : AggregateSubject
{
	private List<Subject> _items = new List<Subject>();
	public Iterator CreateIterator()
	{
		return new IteratorStaff(this);
	}
	public Subject this[int index]
	{
		get { return _items[index]; }
	}
	public void Add(Subject o)
	{
		_items.Add(o);
	}
	public byte Count()
	{
		return (byte)_items.Count;
	}
}
```
<br/><br/>
#### Aggregate Students

```cs
public class Student : AggregateSubject
{
	private List<Subject> _items = new List<Subject>();
	public Iterator CreateIterator()
	{
		return new IteratorStudent(this);
	}
	public Subject this[int index]
	{
		get { return _items[index]; }
	}
	public void Add(Subject o)
	{
		_items.Add(o);
	}
	public byte Count()
	{
		return (byte)_items.Count;
	}
}
``` 
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>

### Iterators

The **Iterator** interface is implemented in two classes, with code baring heavy resemblense.  

```cs
public class IteratorStaff : Iterator
{
	private Staff _aggregate;
	private int _position;

	public IteratorStaff(Staff aggregate)
	{
		_aggregate = aggregate;
		_position = 0;
	}

	public Subject Current()
	{
		if (_position < _aggregate.Count()) return _aggregate[_position];
		return null;
	}

	public Subject First()
	{
		_position = 0;
		return Current();
	}

	public bool IsDone()
	{
		return _position >= _aggregate.Count();
	}

	public Subject Next()
	{
		_position++;
		return Current();
	}
}
```

```cs
public class IteratorStudent : Iterator
{
	private Student _aggregate;
	private int _position;

	public IteratorStudent(Student aggregate)
	{
		_aggregate = aggregate;
		_position = 0;
	}

	public Subject Current()
	{
		if (_position < _aggregate.Count()) return _aggregate[_position];
		return null;
	}

	public Subject First()
	{
		_position = 0;
		return Current();
	}

	/* The rest remains identical to class above
 	 *
     */
}
```

### Implementation  
The above classes complete the **Iterator design pattern**, and are enough to see the implementation of a bloated application.  
We create the **Client** class to encapsulate all of the funtionality brought foward by the pattern mechsnics.  

```cs
public class Client
{
	private AggregateSubject _staff;
	private AggregateSubject _student;

	public Client(AggregateSubject staff, AggregateSubject student)
	{
		_staff = staff;
		_student = student;
	}

	public void Report(Utensil.T target = Utensil.T.ALL)
	{
		List<Iterator> records = new List<Iterator>();
		switch (target)
		{
			case Utensil.T.ALL:
				if (_staff.Count() > 0) records.Add(_staff.CreateIterator());
				if (_student.Count() > 0) records.Add(_student.CreateIterator());
				break;
			default:
				if (target == Utensil.T.STAFF && _staff.Count() > 0) 
					records.Add(_staff.CreateIterator());
				if (target == Utensil.T.STUDENT && _student.Count() > 0) 
					records.Add(_student.CreateIterator());
				break;
		}

		if (records.Count == 0)
		{
			Console.WriteLine("Records Empty.");
			Console.ReadKey();
			return;
		}
		Console.Clear();
		Console.WriteLine("University Records");
		Console.WriteLine("==================");
		foreach (Iterator focus in records)
		{
			Subject item = focus.First();
			while (!focus.IsDone())
			{
				Console.WriteLine(item.ToString());
				item = focus.Next();
			}
		}
		Console.ReadKey();
	}

	public void Capture()
	{
		switch (Utensil.Capture())
		{
			case 0: break;
			case 1: _staff.Add(Data((byte)Utensil.T.STAFF - 1)); break;
			case 2: _student.Add(Data((byte)Utensil.T.STUDENT - 1)); break;
		}
	}
```  

Continues on next page...

```cs
	private Subject Data(byte target)
	{
		Console.Clear();
		string[] subject = { "Staff Member", "Student" };
		string prompt = String.Format("{0} Details", subject[target]);
		Console.WriteLine("{0}\n{1}\n", prompt, "".PadRight(prompt.Length, '='));

		Console.Write("{0,-17}: ", "First Name");
		string fname = Console.ReadLine();
		Console.Write("{0,-17}: ", "Last Name");
		string lname = Console.ReadLine();
		Console.Write("{0,-17}: ", "Age");
		byte age = byte.Parse(Console.ReadLine());
		Console.Write("{0,-17}: ", "Department");
		string dept = Console.ReadLine();
		string field = "";
		byte years = 0;

		switch (target)
		{
			case (byte)Utensil.T.STAFF - 1:
				Console.Write("{0,-17}: ", "Job Description");
				field = Console.ReadLine();
				Console.Write("{0,-17}: ", "Years of Service");
				years = byte.Parse(Console.ReadLine());
				break;

			case (byte)Utensil.T.STUDENT - 1:
				Console.Write("{0,-17}: ", "Qualification");
				field = Console.ReadLine();
				Console.Write("{0,-17}: ", "Year of Study");
				years = byte.Parse(Console.ReadLine());
				break;
		}
		return new Subject(fname, lname, age, dept, field, years);
	}
}
```  

Finally, we get to see how this playsout in the *Main()*.  

```cs
class Program
{
	static void Main(string[] args)
	{
		Client focus = new Client(new Staff(), new Student());
		byte choice = Utensil.Pavilion();

		while (choice != 0)
		{
			switch (choice)
			{
				case 1: focus.Report(); break;
				case 2: focus.Capture(); break;
				case 3: Utensil.List(focus); break;
			}
			choice = Utensil.Pavilion();
		}
	}
}
```

## Conclusion
It goes without saying that this is the implementation of the Iterator pattern in its most basic, can be improved.
