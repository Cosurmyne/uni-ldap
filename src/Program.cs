﻿namespace root
{
    class Program
    {
        static void Main(string[] args)
        {
            Client focus = new Client(new Staff(), new Student());
            byte choice = Utensil.Pavilion();

            while (choice != 0)
            {
                switch (choice)
                {
                    case 1: focus.Report(); break;
                    case 2: focus.Capture(); break;
                    case 3: Utensil.List(focus); break;
                }
                choice = Utensil.Pavilion();
            }
        }
    }
}
