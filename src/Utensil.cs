using System;

namespace root
{
    public static class Utensil
    {
        public enum T { ALL = 0, STAFF = 1, STUDENT = 2 };

        public static byte Pavilion()
        {
            string[] prompt = { "Print Report", "Capture", "Targeted View" };
            return Utensil.Prompt("Uni~LDAP", prompt, "Exit");
        }
        public static byte Capture()
        {
            string[] prompt = { "Staff Member", "Student" };
            return Utensil.Prompt("Capture", prompt, "Back");
        }
        public static void List(Client focus)
        {
            string[] prompt = { "Staff Members", "Students" };
            switch (Utensil.Prompt("List", prompt, "Back"))
            {
                case 1: focus.Report(T.STAFF); break;
                case 2: focus.Report(T.STUDENT); break;
            }
        }

        static byte Prompt(string prompt, string[] options, string exit = null)
        {
            Int32 val = new Int32();
            do
            {
                Console.Clear();
                Console.WriteLine("{0}", prompt);
                Console.WriteLine("".PadRight(prompt.Length, '='));
                for (byte i = 1; i <= options.Length; i++) Console.WriteLine("- {0} :: {1}", i, options[i - 1]);
                Console.WriteLine();

                if (exit != null)
                {
                    Console.WriteLine("- {0} :: {1}", 0, exit);
                    Console.WriteLine();
                }
                Console.Write("selection >> ");
                if (!IsInt(Console.ReadLine(), ref val)) continue;
                if (val >= (exit != null ? 0 : 1) && val <= options.Length) break;
            } while (true);
            return (byte)val;
        }
        public static bool IsInt(string input, ref Int32 val)
        {
            try
            {
                val = int.Parse(input);
            }
            catch (System.FormatException)
            {
                return false;
            }
            return true;
        }
    }
}
