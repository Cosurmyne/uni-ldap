using System;
using System.Collections.Generic;

namespace root
{
    public class Client
    {
        private AggregateSubject _staff;
        private AggregateSubject _student;

        public Client(AggregateSubject staff, AggregateSubject student)
        {
            _staff = staff;
            _student = student;
        }

        public void Report(Utensil.T target = Utensil.T.ALL)
        {
            List<Iterator> records = new List<Iterator>();
            switch (target)
            {
                case Utensil.T.ALL:
                    if (_staff.Count() > 0) records.Add(_staff.CreateIterator());
                    if (_student.Count() > 0) records.Add(_student.CreateIterator());
                    break;
                default:
                    if (target == Utensil.T.STAFF && _staff.Count() > 0) records.Add(_staff.CreateIterator());
                    if (target == Utensil.T.STUDENT && _student.Count() > 0) records.Add(_student.CreateIterator());
                    break;
            }

            if (records.Count == 0)
            {
                Console.WriteLine("Records Empty.");
                Console.ReadKey();
                return;
            }
            Console.Clear();
            Console.WriteLine("University Records");
            Console.WriteLine("==================");
            foreach (Iterator focus in records)
            {
                Subject item = focus.First();
                while (!focus.IsDone())
                {
                    Console.WriteLine(item.ToString());
                    item = focus.Next();
                }
            }
            Console.ReadKey();
        }

        public void Capture()
        {
            switch (Utensil.Capture())
            {
                case 0: break;
                case 1: _staff.Add(Data((byte)Utensil.T.STAFF - 1)); break;
                case 2: _student.Add(Data((byte)Utensil.T.STUDENT - 1)); break;
            }
        }
        private Subject Data(byte target)
        {
            Console.Clear();
            string[] subject = { "Staff Member", "Student" };
            string prompt = String.Format("{0} Details", subject[target]);
            Console.WriteLine("{0}\n{1}\n", prompt, "".PadRight(prompt.Length, '='));

            Console.Write("{0,-17}: ", "First Name");
            string fname = Console.ReadLine();
            Console.Write("{0,-17}: ", "Last Name");
            string lname = Console.ReadLine();
            Console.Write("{0,-17}: ", "Age");
            byte age = byte.Parse(Console.ReadLine());
            Console.Write("{0,-17}: ", "Department");
            string dept = Console.ReadLine();

            string field = "";
            byte years = 0;

            switch (target)
            {
                case (byte)Utensil.T.STAFF - 1:
                    Console.Write("{0,-17}: ", "Job Description");
                    field = Console.ReadLine();
                    Console.Write("{0,-17}: ", "Years of Service");
                    years = byte.Parse(Console.ReadLine());
                    break;

                case (byte)Utensil.T.STUDENT - 1:
                    Console.Write("{0,-17}: ", "Qualification");
                    field = Console.ReadLine();
                    Console.Write("{0,-17}: ", "Year of Study");
                    years = byte.Parse(Console.ReadLine());
                    break;
            }
            return new Subject(fname, lname, age, dept, field, years);
        }
    }
}
