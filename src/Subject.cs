using System;

namespace root
{
    public class Subject
    {
        private string fName;
        private string lName;
        private byte age;
        private string dept;
        private string field;
        private byte years;

        public Subject(string fName, string lName, byte age, string dept, string field, byte years)
        {
            this.fName = fName;
            this.lName = lName;
            this.age = age;
            this.dept = dept;
            this.field = field;
            this.years = years;
        }

        public string FirstName { get{ return fName; } }
        public string LastName { get{ return lName; } }
        public byte Age { get{ return age; } }
        public string Department { get{return dept; } }
        public string Field { get{ return field; } }
        public byte Years { get{ return years; } }

	public override string ToString()
	{
		return String.Format("{0, -20} {1, -20} ({2}) : {3}", FirstName, LastName, Age, Department);
	}
    }
}
