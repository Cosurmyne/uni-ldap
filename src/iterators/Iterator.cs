namespace root
{
    public interface Iterator
    {
        Subject First();
        Subject Next();
        Subject Current();
        bool IsDone();
    }
}
