namespace root
{
    public class IteratorStaff : Iterator
    {
        private Staff _aggregate;
        private int _position;

        public IteratorStaff(Staff aggregate)
        {
            _aggregate = aggregate;
            _position = 0;
        }

        public Subject Current()
        {
            if (_position < _aggregate.Count()) return _aggregate[_position];
            return null;
        }

        public Subject First()
        {
            _position = 0;
            return Current();
        }

        public bool IsDone()
        {
            return _position >= _aggregate.Count();
        }

        public Subject Next()
        {
            _position++;
            return Current();
        }
    }
}
