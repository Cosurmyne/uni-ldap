using System.Collections.Generic;

namespace root
{
    public class Student : AggregateSubject
    {
        private List<Subject> _items = new List<Subject>();
        public Iterator CreateIterator()
        {
		return new IteratorStudent(this);
        }
        public Subject this[int index]
        {
            get { return _items[index]; }
        }
	public void Add(Subject o)
	{
		_items.Add(o);
	}
	public byte Count()
	{
		return (byte)_items.Count;
	}
    }
}
