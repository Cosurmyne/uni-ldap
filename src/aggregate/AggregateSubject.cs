namespace root
{
    public interface AggregateSubject
    {
        Iterator CreateIterator();
	byte Count();
	void Add(Subject o);
    }
}
